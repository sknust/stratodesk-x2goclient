# Stratodesk x2goclient

Tools to get x2goclient running on Stratodesk

## Getting started

- stratodesk-deb-mirror can be run daily by cron to provide current base packages to your own mirror
- x2goclient-installer can be adjusted and used as described in the script to add x2goclient to the Stratodesk ThinClients

## Support
Feel free to contact phyadmin@physik.uni-bielefeld.de for best-effort support. Alternatively, open a ticket in Gitlab.


## Contributing
I am open to contributions. Please submit pull requests.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
This project is licensed under Creative Commons Attribution 4.0 International (https://creativecommons.org/licenses/by/4.0/).

## Project status
This project can be considered done for now. In case updates to the used packages and/or Stratodesk software break compatibility, I will most likely fix any issues.
